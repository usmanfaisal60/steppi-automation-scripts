var axios = require('axios');
var randomEmail = require('random-email');

let body = {
    name: "testUser",
    password: "12345678a",
    gender: "1",
    dob: "1992-07-22"
}

var config = {
    method: 'post',
    url: `${process.env.HOST_URL}auth/register`,
    headers: {
        'Content-Type': 'application/json'
    }
};

module.exports = async (index) => {
    try {
        console.log('[RUNNING REGISTER USER]');
        body.name = "testUser" + index;
        body.phoneNumber = "+92322" + Math.floor(1000000 + Math.random() * 9000000);
        body.email = randomEmail({ domain: 'mailinator.com' });

        config.data = JSON.stringify(body)

        const { data: registerData } = await axios(config)
        console.log("   User registerd successfully")
        let data = JSON.parse(config.data)
        data.regToken = registerData.data.regToken
        data.otp = registerData.data.otpDetails.otp
        data.deviceType = process.env.DEVICE_TYPE
        data.deviceId = process.env.DEVICE_ID
        return data;
    } catch (e) {
        console.log('[RUNNING REGISTER TEST REQUEST FAILED]', e);
    }
}
