var axios = require('axios');

var config = {
    method: 'post',
    url: `${process.env.HOST_URL}auth/verify-account`,
    headers: {
        'Content-Type': 'application/json'
    }
};

module.exports = async (body) => {
    config.data = JSON.stringify(body)
    try {
        console.log('[RUNNING USER OTP VERIFY]');
        const { data: registerData } = await axios(config)
        console.log("   OTP verified")
        return registerData.data.accessToken
    } catch (e) {
        console.log('[RUNNING USER OTP VERIFY TEST REQUEST FAILED]', e);
    }
}
