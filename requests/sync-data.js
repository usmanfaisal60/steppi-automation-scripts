var axios = require('axios');
var config = {
    method: 'post',
    url: 'http://localhost:3000/api/v1/health/sync-now',
    headers: {
        'lang': 'en',
        'timezone': 'Asia/Karachi',
        'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aSI6IjYxMzA1MWRkLWFhYmUtNDA5NC1hMmQxLTM4Njk5NDBiMzNiYSIsImF0IjoiJDJhJDEwJFRkb3VCZW83R1JTb2g5UENFNkJRdC5samlaQjZ1NWdyWXp3aVVNR2xKaEZ5YktQOXdMYnp5Iiwic2kiOiJlZDU4MWU0Ni02ZTc2LTQ2ZWMtOWFmMi1kZTRmYmVhMDVlNDMiLCJpYXQiOjE2MjE5MzMzODYsImV4cCI6MTY1MzQ2OTM4Nn0.mQpJ1BFNtYNPUx67CqzF5KoX5RvKHckvxPdMPJAuXAQ',
        'Content-Type': 'application/json',
        'appVersion': '638',
        'includeGarmin': 'true',
        'deviceType': 'ios'
    },
    data: null
};

module.exports = async token => {
    config.headers.token = token;
    config.data = JSON.stringify({
        "logs": [
            {
                "activeMinutes": Math.floor(Math.random() * 300),
                "steps": Math.floor(Math.random() * 10000),
                "date": "05-27-2021",
                "provider": "1",
                "distance": Math.floor(Math.random() * 50),
                "calories": Math.floor(Math.random() * 4000)
            }
        ]
    });
    try {
        console.log('[RUNNING SYNC DATA]');
        const { data: userData } = await axios(config);
        console.log("   ", userData.message);
    } catch (e) {
        console.log('[SYNC DATA]', e.response.data);
    }
}
