var axios = require('axios');
var data = JSON.stringify({
    "deviceType": "ios",
    "deviceId": "ew9r834h98hr3498hw309hfw93h",
    "phoneNumber": "+923125332963",
    "password": "12345678a"
});

var config = {
    method: 'post',
    url: 'http://localhost:3000/api/v1/auth/login',
    headers: {
        'Content-Type': 'application/json'
    }
};


module.exports = async (body) => {
    config.data = JSON.stringify(body)
    try {
        console.log('[RUNNING LOGIN USER]');
        const { data: loginData } = await axios(config)
        console.log('[LOGIN DATA]', loginData);
        return loginData;
    } catch (e) {
        console.log('[LOGIN REQUEST FAILED]', e.response.data);
    }
}
