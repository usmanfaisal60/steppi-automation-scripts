var axios = require('axios');

var config = {
    method: 'get',
    url:   `${process.env.HOST_URL}challenge/details/5a0ffa72-d23a-4dbe-a456-d1533c70b70c/`,
    headers: {
        'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aSI6IjYxMzA1MWRkLWFhYmUtNDA5NC1hMmQxLTM4Njk5NDBiMzNiYSIsImF0IjoiJDJhJDEwJGQzTG4uNjhWVExaWDdieDlibTY4MU8zZzhPby5uNElSWDdVbmxGNFpRMHN5dXVycHM3Z3dXIiwic2kiOiI3N2M3NTBmYy05MjQzLTQzZDMtODdlYi00OGJhZGVkNTIwYTciLCJpYXQiOjE2MTg4MjUwMjgsImV4cCI6MTY1MDM2MTAyOH0.oplYF9XWpDely4MvKooMYHq-UGMYy0vZP82ZXupUeU0'
    }
};

module.exports = async token => {
    config.headers.token = token;
    try {
        console.log('[GETTING CHALLENGE DETAILS]');
        const { data: challengeData } = await axios(config)
        console.log('[CHALLENGE DATA]', challengeData);
        return challengeData;
    } catch (e) {
        console.log('[GETTING CHALLENGE DETAILS REQUEST FAILED]', e.response.data);
    }
}