var axios = require('axios');

var config = {
    method: 'get',
    url: `http://localhost:3000/api/v1/challenge/join/${process.env.CHALLENGE_ID}`,
    headers: {
        'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aSI6ImZkZDJhYWMwLWRkMzYtNGQzMy04ZDRkLTI5MDI3MzM4YjkzZSIsImF0IjoiJDJhJDEwJGFuOS5Qd21MYmFwMkhpU0k5eU9PUU9RWS9ldkk1aXFJbzFpUXdib3llWkswYkVEU0VxaXpXIiwic2kiOiJjYmI0OTJmNS1kYTYyLTRhZTktODY3ZS05YmQzM2ZmODYwZDEiLCJpYXQiOjE2MjE5Mjg5OTgsImV4cCI6MTY1MzQ2NDk5OH0.-wNkfxqnvpn9E9Zmsy0u9H5cmqT3A4PEXsQ8DZDrImY'
    }
};

module.exports = async token => {
    config.headers.token = token;
    try {
        console.log('[JOINING PUBLIC CHALLENGE ]');
        const { data: challengeData } = await axios(config);
        console.log("   Challenge joined successfully")
        return challengeData;
    } catch (e) {
        console.log('[JOINING PUBLIC CHALLENGE ]', e.response.data);
    }
}
