require('dotenv').config();
const login = require("./requests/login");
const registerUser = require('./requests/register-user');
const syncNow = require('./requests/sync-data');
const joinChallenge = require('./requests/join-public-challenge');
const verifyOtp = require('./requests/verify-otp');

const createUserAndJoinPublicChallenge = async (number) => {
    for (var i = 0; i < number; i++) {
        console.log("----------------", i + 1, "----------------")

        const regData = await registerUser(i + 1);
        if (regData) {
            const token = await verifyOtp(regData);
            await joinChallenge(token);
            await syncNow(token);
        }
        console.log("----------------END--------------")
    }
}

createUserAndJoinPublicChallenge(55);